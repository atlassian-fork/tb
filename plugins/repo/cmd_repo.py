import os
import sys
from typing import Set

from cement import Controller, ex

from tb import Repository
from tb.arguments import repository
from tb.job_execution import parallel_call
from tb import Tb


class RepoController(Controller):
    class Meta:
        label = 'repo'
        help = "commands to manage your teams' git repositories"
        stacked_on = 'base'
        stacked_type = 'nested'
        description = "Commands to help with git repository management functions.  " \
                      "Default command syncs all repositories."

        arguments = repository.arguments

    @repository.validate
    @ex(help="pulls (with rebase) or clones the repositories to your source directory.",
        arguments=repository.arguments)
    def sync(self):
        app: Tb = self.app
        repos = self._get_unique_repos()
        if not repos:
            app.term.info("Nothing to do, exiting")
            return

        names, outputs = self._sync_repos(app.src_dir, repos)
        app.term.columns(names, outputs, ['Repo', 'Sync Status'], report_format="{:32}{:^7}")

    def _default(self):
        self.sync()

    def _get_unique_repos(self) -> Set[Repository]:
        app: Tb = self.app
        team_repos = {r for r in app.repositories.values() if r.team in app.config_manager.selected_teams}
        if app.config_manager.org_repo.name not in (x.name for x in team_repos):
            team_repos.add(app.config_manager.org_repo)
        return team_repos

    def _gen_call_clone(self, src, repo: Repository, branch=None):
        app: Tb = self.app
        app.term.info("Cloning repository: {}, uri: {}".format(repo.name, repo.url))
        path = os.path.realpath(os.path.join(src, repo.name))
        assert (not os.path.isdir(path))  # checked above
        app.term.action("Clone", path)
        args = ['git', "clone"]
        if branch:
            args.extend(['-b', branch])
        args.extend([repo.url, path])
        return app.cmd_callable(repo.name, *args, cwd=src)

    def _gen_call_sync(self, src: str, repo: Repository):
        app: Tb = self.app
        path = os.path.realpath(os.path.join(src, repo.name))
        app.term.action("Sync", path)
        self._call_update_origin(path, repo)
        args = ['git', 'pull', 'origin', '--prune', '--rebase']
        return app.cmd_callable(repo.name, *args, cwd=path)

    def _call_update_origin(self, path, repo: Repository):
        app: Tb = self.app
        args = ['git', 'config', '--get', 'remote.origin.url']
        url, _, ret = app.captured_cmd(repo.name, *args, cwd=path, shell=True)
        if ret != 0:
            print(app.term.warn("Command {} failed with code {}".format(" ".join(args), ret)))
            return ret

        url = url.strip().decode('utf-8')
        expected = repo.url
        if url != expected:
            app.term.print(f"Update origin: <old>{url}</old> -> <new>{expected}</new>")
            args = ['git', 'config', 'remote.origin.url', expected]
            ret = app.cmd(repo.name, *args, cwd=path)

        sys.stdout.flush()
        return ret

    def _sync_repos(self, src: str, repos: Set[Repository]):
        """Clones all uncloned repositories and does a pull --rebase for others."""
        jobs = []
        for repo in repos:
            path = os.path.realpath(os.path.join(src, repo.name))
            if not os.path.isdir(path):
                jobs.append(self._gen_call_clone(src, repo))
            else:
                jobs.append(self._gen_call_sync(src, repo))

        outputs = parallel_call(jobs)
        return (r.name for r in repos), outputs


